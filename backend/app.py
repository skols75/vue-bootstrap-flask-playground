from flask import Flask, jsonify, request
from flask_cors import CORS
import json
from datetime import datetime as dt

# configuration
DEBUG = True

# instatiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r"/*": {"origins": "*"}})

BOOKS = [
    {
        "title": "On the Road",
        "author": "Jack Kerouac",
        "read": True,
        "last_updated": "3/21/2021",
    },
    {
        "title": "Harry Potter and the Philosopher's Stone",
        "author": "J. K. Rowling",
        "read": False,
        "last_updated": "2/21/2021",
    },
    {
        "title": "Green Eggs and Ham",
        "author": "Dr. Seuss",
        "read": True,
        "last_updated": "2/28/2021",
    },
]


# test route
@app.route("/ping", methods=["GET"])
def ping_pong():
    return jsonify("pong!")


@app.route("/books", methods=["GET", "POST"])
def all_books():
    response_object = {"status": "success"}
    if request.method == "POST":
        title = request.form.get("title")
        author = request.form.get("author")
        read = request.form.get("read")
        pages = request.form.get("filePages")
        # last_updated = request.form.get("lastUpdated")
        last_updated = dt.now()
        # print("dt.now(): ", dt.now())
        post_data_files = request.files.getlist("files")
        print("post_data_files: ", post_data_files)
        print("pages: ", pages)
        print("json.loads(pages)", json.loads(pages))
        p = json.loads(pages)
        for file in post_data_files:
            # print("file", file)
            print("file.filename", file.filename)
            print("p[file.filename]", p[file.filename])
            print("title:", title)
            print("author:", author)
            print("read:", read)
            print("last_updated:", last_updated)
        BOOKS.append(
            {
                "title": title,
                "author": author,
                "read": read,
                "last_updated": last_updated,
            }
        )
        response_object["message"] = "Book added!"
    else:
        response_object["books"] = BOOKS
    return response_object


if __name__ == "__main__":
    app.run()
