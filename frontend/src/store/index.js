import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
  profile: [],
};

const actions = {};

const mutations = {};

const getters = {};

const modules = {};

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules,
});

export default store;
